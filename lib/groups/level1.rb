
class Level1 < GameGnome::GnomeGroup

  def setup
    @bounds = @app.view.bounds
    @starzone = { bottom: 2.0, top: 500.0 }
    @starcount = 1000
    puts "setting up level 1!"
    @starcount.times do
      star = make_object Star
      b = @starzone[:bottom]
      t = @starzone[:top]
      r = rand(b, t)
      star.set_position rand(@bounds[:left]..@bounds[:right]), rand(@starzone[:bottom]..@starzone[:top]) 
    end
    @maru = make_object Maru
    @maru.set_position 0, 0
    color = palette.sample
    13.times do
      sb = make_object(StarBit, color)
      sb.set_position 1, 1
    end
  end

  def palette
    @palette ||= Native( `tinycolor( {h: #{rand() * 100.0}, s: 100, v: 100 }).triad()` )
  end

  def world
    @world
  end

  def start
    puts "starting level 1!"
  end

  def stop
  end

  def update
    return if @maru.destroyed?
    @app.view.y = [0, @maru.y].max
    mbounds = @maru.bounds
    if mbounds[:left] < @bounds[:left] || mbounds[:right] > @bounds[:right] || mbounds[:bottom] < @bounds[:bottom]
      @maru.body.raw.setLinearVelocity(`planck.Vec2.zero()`)
      @maru.pop!
    end
  end

  def input_event event
    if event[:device] == :pointer
      if event[:type] == :down
        pointa = [event[:world_x], event[:world_y]]
        pointb = @maru.position 
        dir = direction(pointb, pointa)
        dis = distance(pointb, pointa)
        @maru.body.raw.applyLinearImpulse(`planck.Vec2(#{(0.1 / dis) * dir[0]},#{(-0.1 / dis) * dir[1]})`, @maru.body.raw.getWorldCenter, true)
        @wind = make_object Wind
        @wind.set_position event[:world_x], event[:world_y]
        @wind.rotation = angle(dir)
      elsif event[:type] == :up
        #puts "up!"
      end
    end

  end


  def distance a, b
    Math.sqrt(((a[0] - b[0]) ** 2) + ((a[1] - b[1]) ** 2))
  end

  def angle vector
    Math.atan2(-vector[1], vector[0])
  end

  def direction a, b, normalize=true
    dist = (normalize) ? distance(a, b) : 1
    [(a[0] - b[0]) / dist, (a[1] - b[1]) / dist]
  end

end

