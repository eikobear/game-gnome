require "browser"

module PIXI

  class Application

    def initialize options
      @app = Native `new PIXI.Application(#{options.to_n})`
      #$document.body << @app[:view].to_n
    end

    def renderer
      @app.renderer
    end

    def ticker
      @app.ticker
    end

    def attach_to selector
      $document.body.at_css(selector) << @app[:view].to_n
    end

    def resize x, y
      @app.renderer.autoResize = true
      @app.renderer.resize(x, y)
    end

    def load_textures textures, &block
      `PIXI.loader.add(#{textures.to_n}).load(#{block})`
    end

    def load_texture_atlas atlas, &block
      `PIXI.loader.add(#{atlas}).load(#{block})`
    end

    def atlas path
      `PIXI.loader.resources[#{path}].textures`
    end

    def texture path
      `PIXI.loader.resources[#{path}].texture`
    end

    def new_sprite texture
      Sprite.new self, texture
    end

    def stage
      @app.stage
    end

  end

  class Sprite

    def initialize app, texture
      @app = app
      @sprite = `new PIXI.Sprite(PIXI.loader.resources[#{texture}].texture)`
      @app.stage.addChild(sprite)
    end

    def visible enabled
      @sprite.visible = enabled
    end

    def texture= texture
      @sprite.texture = `PIXI.loader.resources[#{texture}].texture`
    end

    def x
      @sprite.x
    end

    def y
      @sprite.y
    end

    def x= value
      @sprite.x = value
    end

    def y= value
      @sprite.y = value
    end

    def set_position x, y
      puts "PIXIE SETTING SPRITE POSITION TO #{x}, #{y}"
      @sprite.position.set(x, y)
    end

    def position
      @sprite.position
    end

    def width
      @sprite.width
    end

    def height
      @sprite.height
    end

    def width= value
      @sprite.width = value
    end

    def height= value
      @sprite.height = value
    end

    def scale_x
      @sprite.scale.x
    end

    def scale_y
      @sprite.scale.y
    end

    def scale_x= value
      @sprite.scale.x = value
    end

    def scale_y= value
      @sprite.scale.y = value
    end

    def scale
      @sprite.scale;
    end

    def set_scale xscale, yscale
      @sprite.scale.set(xscale, yscale)
    end

    def rotation
      @sprite.rotation;
    end

    def rotation= radians
      @sprite.rotation= radians
    end

    def anchor
      @sprite.anchor
    end

    def pivot
      @sprite.pivot
    end


  end

end


