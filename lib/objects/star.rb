

class Star < GameGnome::GnomeObject

  def setup
    #puts "setting up Star!"
    set_animation("Twinkle")
    @color = @group.palette.sample
    raw.tint = color_value
  end

  def start
   # puts "starting Star!"
   # @tick = 0
  end

  def update delta
    #@tick += delta
    #if @tick > 10
    #  self.x += rand(3) - 1
    #  self.y += rand(3) - 1
    #  @tick = 0
    #end
  end

  def color
    @color
  end

  def color_value
    (color[:_r] << 16) + (color[:_g] << 8) + (color[:_b])
  end

  def burst!
    13.times do |i|
      sb = @group.make_object(StarBit, @color)
      sb.set_position x, y
    end
    destroy!
  end


end
