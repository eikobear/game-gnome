

class Maru < GameGnome::GnomeObject

  def setup
    puts "setting up Maru!"
    set_animation("Float")
  end

  def start
    puts "starting Maru!"
    @collected = []
    @total = 0
    @tick = 0
    @color = Native(`tinycolor('white')`)
  end

  def update delta
    #self.x = @body.getPosition.x
    #self.y = -@body.getPosition.y
    #@tick += delta
    #if @tick > 10
    #  self.x += rand(3) - 1
    #  self.y += rand(3) - 1
    #  @tick = 0
    #end
  end

  def on_collision them, me, contact
    puts "#{me.class.name} collided with #{them.class.name}"
    if them.class == Star
      @collected << them.color
      @total += 1
      @color = Native(`tinycolor.mix(#{@color.to_n}, #{them.color.to_n}, 13)`)
      raw.scale.set(1 + (@total / 100), 1 + (@total / 100))
      raw.tint = color_value
      them.burst!
      body.raw.applyLinearImpulse(`planck.Vec2(0.0, -0.05)`, body.raw.getWorldCenter, true)
    end
  end

  def color
    @color
  end

  def color_value
    puts "setting r: #{color[:_r]}, g: #{color[:_g]}, b: #{color[:_b]}"
    (color[:_r] << 16) + (color[:_g] << 8) + (color[:_b])
  end

  def pop!
    return if popped?
    set_animation("Pop", false)
    @popped = true
  end

  def animation_end
    destroy!
  end

  def popped?
    @popped ||= false
  end

end
