

class StarBit < GameGnome::GnomeObject

  def loaded color
    @color = color
  end

  def setup
    #puts "setting up Star!"
    set_animation("Twinkle")
    dir = rand() * 2 * Math::PI
    @life = rand(0.5..1.0)
    speed = 1.5
    x = Math.cos(dir) * speed
    y = Math.sin(dir) * speed
    body.raw.destroyFixture(body.raw.getFixtureList())
    body.raw.setLinearVelocity(`planck.Vec2(#{x}, #{y})`)
    raw.tint = color_value(@color)
  end

  def sprite_name
    "Star"
  end

  def start
   @tick = 0
  end

  def update delta
    @tick += delta
    scale = (@life - @tick) / @life
    @sprite.scale.set(scale, scale)
    if @tick > @life
      destroy!
    end
  end

  def color_value(c)
    (c[:_r] << 16) + (c[:_g] << 8) + (c[:_b])
  end


end
