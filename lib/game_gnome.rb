require_relative "pixi_wrapper"
require_relative "gnome_physics"

module GameGnome

  def self.new_game options, &block
    $document.ready do
      game = GnomeGame.new options
      yield game
    end
  end

  class GnomeObject

    @@descendants = []

    def self.inherited(subclass)
      @@descendants << subclass
    end

    def self.descendants
      @@descendants
    end

    def body= body
      @body = body
    end

    def body
      @body
    end

    def x
      body.x
    end

    def y
      body.y
    end

    def x= value
      body.x = value
    end

    def y= value
      body.y = -value
    end

    def initialize app, group
      @app = app
      @group = group
      @name = self.class.name
      @frames = {}
      @animations = []
    end

    def sprite_name
      @name
    end

    def set_position x, y
      body.set_position(x, y)
    end

    def position
      [self.x, self.y]
    end

    def bounds
      bounds = @sprite.getLocalBounds
      x = @sprite[:position].x / @app.world.pixel_scale
      y = @sprite[:position].y / @app.world.pixel_scale
      {
        right: bounds.right / @app.world.pixel_scale + x,
        top: -bounds.top / @app.world.pixel_scale - y,
        left: bounds.left / @app.world.pixel_scale + x,
        bottom: -bounds.bottom / @app.world.pixel_scale - y,
        center: [x, y]
      }
    end

    def set_animation animation, should_loop = true
      s = Native @sprite
      s.loop = should_loop
      s[:textures] = @frames[animation]
      s.play()
    end

    def on_loop event
      animation_loop(event)
    end

    def on_complete event
      animation_end(event)
    end

    def animation_loop event
    end

    def animation_end event
    end

    def on_load *args
      @frames = @group.animations_for sprite_name
      @animations = @frames.keys
      @sprite = Native(`new PIXI.extras.AnimatedSprite(#{@frames[@animations[0]]})`)
      @sprite[:onLoop] = Proc.new do |event|
        on_loop(event)
      end
      @sprite[:onComplete] = Proc.new do |event|
        on_complete(event)
      end
      @app.add_object(self)
      @sprite.animationSpeed = 0.08;
      self.anchor 0.5, 0.5
      @sprite.play()
      loaded *args
    end

    def loaded
    end

    def anchor x, y
      @sprite.anchor.x = x
      @sprite.anchor.y = y
    end

    def raw
      @sprite
    end

    def rotation
      @sprite.rotation
    end

    def rotation= value
      @sprite.rotation = value
    end


    def setup *args
    end

    def start
    end

    def stop
    end

    def on_update delta
      @sprite.position.set(body.x * body.world.pixel_scale, -body.y * body.world.pixel_scale)
      update delta
    end

    def update delta
    end

    def destroy!
      @body.world.raw.destroyBody(@body.raw)
      @sprite.destroy
      @destroyed = true
    end

    def destroyed?
      @destroyed ||= false
    end

  end


  class GnomeGame

    def initialize options
      @app = PIXI::Application.new options
      @app.attach_to("#pixi-application")
      @ticker_ms = 0
      @app.ticker.add do |delta|
        delta = (delta / 0.06) / 1000.0
        on_update delta
      end
      @world = GnomePhysics::World.new
      @groups = []

      @view = GnomeView.new self
      @app.stage.addChild(@view.raw)

      @app.renderer.plugins.interaction.on("pointerdown") do |event|
        @groups.each { |group| group.on_pointerdown event }
      end

      @app.renderer.plugins.interaction.on("pointerup") do |event|
        @groups.each { |group| group.on_pointerup event }
      end

      #TODO find out why this is the same as pointerup
      #@app.renderer.plugins.interaction.on("pointertap") do |event|
      #  @groups.each { |group| group.on_pointertap event }
      #end

    end

    def view
      @view
    end

    def world
      @world
    end

    def raw
      @app
    end

    def width
      @app.renderer.width / @world.pixel_scale
    end

    def height
      @app.renderer.height / @world.pixel_scale
    end

    def pixelwidth
      @app.renderer.width
    end

    def pixelheight
      @app.renderer.height
    end

    def load_group group_class, &block
      if GnomeGroup.descendants.include? group_class
        group = group_class.new self
        @groups << group
        group.load &block
        return group
      else
        puts "ERROR: could not find a GnomeGroup named #{group_class}. did you forget to inherit GameGnome::GnomeGroup?"
        return nil
      end
    end

    def load_object group, object_class
      if GnomeObject.descendants.include? object_class
        object = object_class.new self, group
        object.body = @world.create_body object: object
        return object
      else
        puts "ERROR: could not find a GnomeObject named #{object_class}. did you forget to inherit GameGnome::GnomeObject?"
        return nil
      end
    end

    def load_texture_atlas textures, &block
      @app.load_texture_atlas textures, &block
    end

    def add_object object
      @view.raw.addChild(object.raw)
    end

    def stage
      @app.stage
    end

    def on_update delta
      update delta
      world.update(delta)
      @groups.each { |group| group.on_update delta }
    end

    def update delta
    end

  end


  class GnomeView

    def initialize app
      @app = app
      @container = Native(`new PIXI.Container()`)
      set_position 0, 0
    end

    def set_position(x, y)
      self.x = x
      self.y = y
    end

    def bounds
      { 
        right:  self.x + (@app.width / 2),
        top:    self.y + (@app.height / 2),
        left:   self.x - (@app.width / 2),
        bottom: self.y - (@app.height / 2)
      }
    end



    def screen_to_world x, y
      [screen_x_to_world(x), screen_y_to_world(y)]
    end

    def screen_x_to_world x
      self.x + ((x - @app.raw.renderer.width / 2.0) / @app.world.pixel_scale)
    end

    def screen_y_to_world y
      self.y - ((y - @app.raw.renderer.height / 2.0) / @app.world.pixel_scale)
    end

    def y_to_world value
      (value / @app.world.pixel_scale - @app.raw.renderer.height / 2.0)
    end

    def x= value
      @container.x = -value * @app.world.pixel_scale + @app.raw.renderer.width / 2.0
    end

    def x
      -(@container.x - @app.raw.renderer.width / 2.0) / @app.world.pixel_scale
    end

    def y= value
      @container.y = value * @app.world.pixel_scale + @app.raw.renderer.height / 2.0
    end

    def y
      (@container.y - @app.raw.renderer.height / 2.0) / @app.world.pixel_scale
    end

    def raw
      @container
    end

    def zoom= ratio
      @container.scale.x = ratio
      @container.scale.y = ratio
    end

    def zoom
      @container.scale.x
    end

  end

  class GnomeAtlas

    def initialize atlas
      @atlas = atlas
    end

    def on_progress &block
      @progress_callback = block
    end

    def on_complete &block
      @complete_callback = block
    end

    def load
      `PIXI.loader.add(#{@atlas}).on("progress", #{@progress_callback}).on("complete", #{@complete_callback}).load()`
    end

    def textures
      Native `PIXI.loader.resources[#{@atlas}].textures`
    end

  end

  class GnomeGroup

    @@descendants = []

    def self.inherited(subclass)
      @@descendants << subclass
    end

    def self.descendants
      @@descendants
    end

    def initialize app
      @app = app
      @name = self.class.name
      @atlas = GnomeAtlas.new "images/#{@name}/#{@name}.json"
      @loaded = false
      @atlas.on_progress { |loader, resource| on_atlas_progress Native(loader), Native(resource) }
      @atlas.on_complete { |loader| on_atlas_complete Native(loader) }
      @objects = []
      @running = false
    end

    def make_object object_class, *args
      object = @app.load_object self, object_class
      @objects << object unless object.nil?
      object.on_load *args if @loaded
      if @running
        object.setup
        object.start
      end
      return object
    end

    def animations_for object_name
      frames = @atlas.textures
      animations = Hash.new { |h,k| h[k] = Array.new }
      frames.each do |k,v|
        group, object, animation, frame = k.split("/")
        animations[animation][frame.to_i] = v if object == object_name
      end
      return animations
    end

    def load &block
      @on_loaded = block
      @atlas.load 
    end

    def on_atlas_progress loader, resource
      puts " - #{loader[:progress]}%\tloading: #{resource[:name]}"
    end

    def on_atlas_complete loader
      puts " - #{@name} loaded!"
      @loaded = true
      @objects.each { |object| object.on_load }
      @on_loaded.call(self) if @on_loaded
    end

    def start!
      unless @loaded
        puts "ERROR: tried to start Group #{@name} before it finished loading."
        return false
      end
      setup
      @objects.each { |object| object.setup }
      start
      @objects.each { |object| object.start }
      @running = true
    end

    def setup
      #after load
    end

    def start
      #begin
    end

    def stop
      #clean up
    end

    def on_update delta
      return unless @running
      update delta
      @objects = @objects.select do |object| 
        next false if object.destroyed?
        object.on_update delta
        true
      end
    end

    def update delta
      #tick
    end

    def on_pointerdown event
      e = Native(event)
      data = {
        device: :pointer,
        type: :down,
        mouse_x: e.data.global.x,
        mouse_y: e.data.global.y,
        world_x: @app.view.screen_x_to_world(e.data.global.x),
        world_y: @app.view.screen_y_to_world(e.data.global.y),
        raw: e
      }
      self.input_event(data)
    end

    def on_pointerup event
      e = Native(event)
      data = {
        device: :pointer,
        type: :up,
        mouse_x: e.data.global.x,
        mouse_y: e.data.global.y,
        world_x: @app.view.screen_x_to_world(e.data.global.x),
        world_y: @app.view.screen_y_to_world(e.data.global.y),
        raw: e
      }
      self.input_event(data)
    end

    def input_event data
    end

  end

end


#$document.ready do
#  options = { width: 200, height: 200, transparent: true }
#  app = GameGnome::GnomeGame.new options
#  group = GameGnome::GnomeGroup.new app, "main"
#  atlas.on_progress do
#    puts "progress!"
#  end
#  atlas.on_load do
#    puts "loaded!"
#  end
#  atlas.load
#  #GameGnome::GnomeObject.new app, "maru"
#  #app = PIXI::Application.new(options) 
#  #app.attach_to("#pixi-application")
#  #app.load_textures(["images/maru.png"]) do 
#  #  sprite = app.new_sprite("images/maru.png")
#  #end
#end

