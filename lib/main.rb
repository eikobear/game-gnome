
require_relative "game_gnome"

options = { width: 800, height: 600, transparent: true }

GameGnome.new_game(options) do |game|
  level_1 = game.load_group Level1 do |group|
    group.start!
  end
end
