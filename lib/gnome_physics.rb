
module GameGnome

  module GnomePhysics
    class World

      def initialize
        @world = Native `planck.World(planck.Vec2(0.0, 0.0))`
        @callbacks = []
        handler = Proc.new do |contact, old_manifold|
          c = Native(contact)
          obja = c.getFixtureA().getBody().getUserData()[:object]
          objb = c.getFixtureB().getBody().getUserData()[:object]
          @callbacks << Proc.new { obja.on_collision(objb, obja, c) } if obja.respond_to? :on_collision
          @callbacks << Proc.new { objb.on_collision(obja, objb, c) } if objb.respond_to? :on_collision
        end
        @world.on('pre-solve', handler)
        @pixel_scale = 64.0
      end

      def raw
        @world
      end

      def pixel_scale
        @pixel_scale
      end

      def pixel_scale= value
        @pixel_scale = value
      end

      def update delta
        @world.step(delta)
        @callbacks.each { |callback| callback.call }
        @callbacks = []
      end

      def create_body options
        obj = options[:object]
        body = @world.createDynamicBody(`planck.Vec2(0.0, 0.0)`)
        body.setUserData(object: obj)
        Body.new(self, body)
      end

    end

    class Body

      def initialize world, body
        @world = world
        @body = body
        @body.createFixture(`planck.Circle(0.25)`, { density: 0.1 })
      end

      def raw
        @body
      end

      def world
        @world
      end

      def position
        pos = @body.getPosition
        [pos.x * world.pixel_scale, -pos.y]
      end

      def set_position x, y
        pos = @body.getPosition
        pos.x = x
        pos.y = -y
        @body.setPosition(pos)
      end

      def x= value
        pos = @body.getPosition
        pos.x = value
        @body.setPosition(pos)
      end

      def y= value
        pos = @body.getPosition
        pos.y = -value
        @body.setPosition(pos)
      end

      def x
        @body.getPosition.x
      end

      def y
        -@body.getPosition.y
      end

    end
  end
end

