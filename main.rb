require "sinatra"
require "opal"
require "opal-browser"


opal_lib = Opal::Builder.build("opal")
get "/opal.js" do
  content_type "text/javascript"
  opal_lib.to_s
end

opal_browser_lib = Opal::Builder.build("opal-browser")
get "/opal-browser.js" do
  content_type "text/javascript"
  opal_browser_lib.to_s
end

get "/scripts/app.js" do
  content_type "text/javascript"
  builder = Opal::Builder.new
  builder.append_paths("lib")
  builder.build("main.rb")
  groups = Dir["./lib/groups/*.rb"]
  groups.each { |group| builder.build(group) }
  objects = Dir["./lib/objects/*.rb"]
  objects.each { |object| builder.build(object) }
  builder.to_s
end

# file path interpolation is likely insecure
#get "/scripts/:file" do
#  content_type = "text/javascript"
#  raw_script = File.read("./scripts/#{params[:file]}")
#  parsed_script = Opal.parse(raw_script);
#  parsed_script
#end

get "/" do
  erb :index
end
